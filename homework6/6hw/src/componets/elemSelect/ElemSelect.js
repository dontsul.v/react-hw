import { AiFillStar } from 'react-icons/ai';
import { useDispatch } from 'react-redux';
import { toggleSelected } from '../../slices/autoSelectedSlice';
import styles from './elemSelect.module.scss';
const ElemSelect = (props) => {
    const { id, name, price, img, color } = props.auto;
    const dispatch = useDispatch();
    return (
        <li className={styles.elemSelect}>
            <div>
                <img className={styles.img} src={img} alt="auto" />
            </div>
            <div>
                <h4>{name}</h4>
                <p>Color: {color}</p>
            </div>
            <div>
                <h5>Price: {price}$</h5>
            </div>

            <div>
                <AiFillStar
                    className={styles.activeStarS}
                    onClick={(e) => {
                        dispatch(toggleSelected(props.auto));
                    }}
                />
            </div>
        </li>
    );
};

export default ElemSelect;
