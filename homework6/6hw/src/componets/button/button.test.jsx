import { Button } from './Button';
import { fireEvent, render, screen } from '@testing-library/react';

test('render button in modal', () => {
    const { getByText, asFragment } = render(<Button text="Ok" />);
    const linkElement = getByText(/Ok/i);
    expect(asFragment(<Button />)).toMatchInlineSnapshot(`
<DocumentFragment>
  <div
    class="wrap-btn"
  >
    <button
      class="btn"
      style="margin: 10px;"
    >
      Ok
    </button>
  </div>
</DocumentFragment>
`);
    // expect(linkElement).toBeInTheDocument();
});
