import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import styles from './button.module.scss';

export function Button(props) {
    const {
        backgroundColor = '',
        text = '',
        handlModal = Function.prototype,
        addToBasket = Function.prototype,
        deleteFromBasket = Function.prototype,
    } = props;

    return (
        <div className="wrap-btn">
            <button
                className={styles.btn}
                style={{
                    backgroundColor: `${backgroundColor}`,
                    margin: '10px',
                }}
                onClick={() => {
                    addToBasket();
                    deleteFromBasket();
                    handlModal();
                }}
            >
                {text}
            </button>
        </div>
    );
}

// Button.propTypes = {
//     backgroundColor: PropTypes.string.isRequired,
//     text: PropTypes.string.isRequired,
//     handlModal: PropTypes.func,
//     addToBasket: PropTypes.func,
//     autoState: PropTypes.object,
// };
