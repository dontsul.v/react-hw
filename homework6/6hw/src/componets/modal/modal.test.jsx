import Modal from './Modal';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('modal', () => {
    test('exist modal', () => {
        const { asFragment } = render(
            <Provider store={store}>
                <Modal header="Modal" />
            </Provider>
        );
        expect(asFragment(<Modal />)).toMatchInlineSnapshot(
            `<DocumentFragment />`
        );
    });
});
