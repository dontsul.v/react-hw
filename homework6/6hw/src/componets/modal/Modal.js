import { useRef } from 'react';
import PropTypes from 'prop-types';
import { CgCloseR } from 'react-icons/cg';
import styles from './modal.module.scss';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { toggleModalStatus } from '../../slices/autoBasketSlice';
import { Button } from '../button/Button';

function Modal(props) {
    const modal = useRef(null);
    const { header, closeButton, text, handleBasket } = props;
    const modalStatus = useSelector((state) => state.autoBasket.isModal);
    const dispatch = useDispatch();

    return (
        modalStatus && (
            <div
                ref={modal}
                onClick={(e) => {
                    if (e.target === modal.current) {
                        dispatch(toggleModalStatus(false));
                    }
                }}
                className={styles.modalBackground}
            >
                <div className={styles.modal}>
                    <div className={styles.modalContent}>
                        <h3 className={styles.title}>{header}</h3>
                        {closeButton && (
                            <CgCloseR
                                onClick={() => {
                                    dispatch(toggleModalStatus(false));
                                }}
                                className={styles.icon}
                            />
                        )}
                        <p className={styles.text}>{text}</p>
                        <div className={styles.btnGroup}>
                            <Button
                                backgroundColor="#d44637"
                                text="Ok"
                                addToBasket={() => {
                                    dispatch(toggleModalStatus(false));
                                    handleBasket();
                                }}
                            />
                            <Button
                                backgroundColor="#d44637"
                                text="Cancel"
                                handlModal={() =>
                                    dispatch(toggleModalStatus(false))
                                }
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    );
}

export default Modal;

// Modal.propTypes = {
//     header: PropTypes.string.isRequired,
//     closeButton: PropTypes.bool.isRequired,
//     text: PropTypes.string.isRequired,
//     // viewModal: PropTypes.bool.isRequired,
//     // handlModal: PropTypes.func.isRequired,
//     // actions: PropTypes.element.isRequired,
// };
