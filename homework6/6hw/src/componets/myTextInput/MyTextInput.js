import { useField } from 'formik';
import styles from './myTextInput.module.scss';
export const MyTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);

    return (
        <>
            <label htmlFor={props.name}>{label}</label>
            <input {...field} {...props} />
            {meta.touched && meta.error ? <div className={styles.error}>{meta.error}</div> : null}
        </>
    );
};
