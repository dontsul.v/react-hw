import { createContext, useState } from 'react';
import { ListAutos } from '../listAutos/ListAutos';

export const MyContext = createContext('default context');

const Home = () => {
  const [cardsFormat, setCardsFormat] = useState(true);
  const [tableFormat, setTableFormat] = useState(false);

  const handleFormat = (e) => {
    if (e.target.dataset.format === 'cards' && cardsFormat !== true) {
      setCardsFormat(!cardsFormat);
      setTableFormat(!tableFormat);
    } else if (e.target.dataset.format === 'table' && tableFormat !== true) {
      setCardsFormat(!cardsFormat);
      setTableFormat(!tableFormat);
    }
  };

  const listFormatDatas = {
    cardsFormat,
    tableFormat,
    handleFormat,
  };

  return (
    <div>
      <MyContext.Provider value={listFormatDatas}>
        <ListAutos />
      </MyContext.Provider>
    </div>
  );
};

export default Home;
