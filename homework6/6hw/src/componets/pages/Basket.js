import Modal from '../modal/Modal';
import ElemBasket from '../elemBasket/ElemBasket';
import styles from './basket.module.scss';
import { useSelector, useDispatch } from 'react-redux';
import { deleteFromBasket, toggleModalStatus } from '../../slices/autoBasketSlice';
import { CustomForm } from '../customForm/CustomForm';
const Basket = (props) => {
    const cartEmpty = <h2 style={{ textAlign: 'center' }}>Cart is empty</h2>;
    const autosBasket = useSelector((state) => state.autoBasket.itemsBasket);

    const dispatch = useDispatch();
    const auto = useSelector((state) => state.autoState.autoState);
    return (
        <div className="container">
            <div>
                <div>
                    {autosBasket.length !== 0 ? (
                        <ul className={styles.listAutos}>
                            {autosBasket.map((elem) => {
                                return <ElemBasket key={elem.id} auto={elem} />;
                            })}
                        </ul>
                    ) : (
                        cartEmpty
                    )}

                    {
                        <Modal
                            header="Modal"
                            closeButton={true}
                            text="
                            Are you sure you want to delete this element?"
                            viewModal={toggleModalStatus}
                            handleBasket={() => {
                                dispatch(deleteFromBasket(auto));
                            }}
                        />
                    }
                </div>
                <div>
                    <CustomForm />
                </div>
            </div>
        </div>
    );
};

export default Basket;
