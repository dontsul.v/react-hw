import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchData = createAsyncThunk('autos/getAutos', async () => {
    const res = await fetch('MOCK_DATA.json');
    const autos = await res.json();
    return autos;
});

const autosSlice = createSlice({
    name: 'autos',
    initialState: {
        items: [],
        loading: 'idle',
        error: null,
    },

    extraReducers: (builder) => {
        builder
            .addCase(fetchData.pending, (state, action) => {
                state.loading = 'loading';
            })
            .addCase(fetchData.rejected, (state, action) => {
                state.loading = 'idle';
                state.error = 'Error';
            })
            .addCase(fetchData.fulfilled, (state, action) => {
                state.items = action.payload;
                state.loading = 'idle';
            });
    },
});

export const autosReducer = autosSlice.reducer;
