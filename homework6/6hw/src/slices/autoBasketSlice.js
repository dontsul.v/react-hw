import { createSlice } from '@reduxjs/toolkit';

const autoBasketSlice = createSlice({
    name: 'autoBasket',
    initialState: {
        itemsBasket: [],
        loading: 'idle',
        error: null,
        isModal: false,
    },
    reducers: {
        addInBasket: (state, action) => {
            const res = state.itemsBasket.findIndex(
                (obj) => obj.id === action.payload.id
            );

            if (res === -1) {
                state.itemsBasket.push({ ...action.payload, quantity: 1 });
                localStorage.setItem(
                    'autosBasket',
                    JSON.stringify([...state.itemsBasket])
                );
            } else {
                state.itemsBasket[res].quantity =
                    state.itemsBasket[res].quantity + 1;
                localStorage.setItem(
                    'autosBasket',
                    JSON.stringify([...state.itemsBasket])
                );
            }
        },
        deleteFromBasket: (state, action) => {
            state.itemsBasket = state.itemsBasket.filter(
                (elem) => elem.id !== action.payload.id
            );
            localStorage.setItem(
                'autosBasket',
                JSON.stringify([...state.itemsBasket])
            );
        },
        toggleModalStatus: (state, action) => {
            state.isModal = action.payload;
        },
        setItemsBasket: (state, action) => {
            state.itemsBasket = action.payload;
        },

        clearBasket: (state) => {
            localStorage.setItem('autosBasket', []);
            state.itemsBasket = [];
        },
    },
});

export const autoBasketReducer = autoBasketSlice.reducer;
export const {
    addInBasket,
    toggleModalStatus,
    deleteFromBasket,
    setItemsBasket,
    clearBasket,
} = autoBasketSlice.actions;
