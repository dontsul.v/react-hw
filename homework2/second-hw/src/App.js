import Button from './componets/button/Button';
import Modal from './componets/modal/Modal';
import styles from './App.module.scss';
import { Component } from 'react';
import { ListAutos } from './componets/listAutos/ListAutos';
import { Header } from './componets/header/Header';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            autos: [],
            autosBasket: [],
            selectAutos: [],
            autoState: {},
            viewFirstModal: false,
        };
    }

    componentDidMount() {
        fetch('MOCK_DATA.json')
            .then((res) => res.json())
            .then((data) => {
                this.setState({ autos: data });
            });
        const selectAutosStorage = localStorage.getItem('selectAutos');
        if (selectAutosStorage) {
            this.setState({ selectAutos: JSON.parse(selectAutosStorage) });
        }
        const basketAutosStorage = localStorage.getItem('autosBasket');
        if (basketAutosStorage) {
            this.setState({ autosBasket: JSON.parse(basketAutosStorage) });
        }
    }

    addToSelected = (auto, id) => {
        const res = this.state.selectAutos.findIndex((obj) => obj.id === id);
        if (res === -1) {
            this.setState({ selectAutos: [...this.state.selectAutos, auto] });
            localStorage.setItem('selectAutos', JSON.stringify([...this.state.selectAutos, auto]));
        } else {
            const arr = this.state.selectAutos.filter((isAuto) => isAuto.id !== id);
            this.setState({ selectAutos: [...arr] });
            localStorage.setItem('selectAutos', JSON.stringify(arr));
        }
    };

    addToBasket = (auto) => {
        this.setState({ autosBasket: [...this.state.autosBasket, auto] });

        localStorage.setItem('autosBasket', JSON.stringify([...this.state.autosBasket, auto]));
    };

    addAuto = (auto) => {
        this.setState({ autoState: auto });
    };

    handleFirstModal = (e) => {
        this.setState({
            viewFirstModal: !this.state.viewFirstModal,
        });
    };

    render() {
        const btnF = (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Button
                    addToBasket={this.addToBasket}
                    text="Ok"
                    backgroundColor="#d44637"
                    handlModal={this.handleFirstModal}
                    autoState={this.state.autoState}
                />
                <Button
                    text="Cancel"
                    backgroundColor="#d44637"
                    handlModal={this.handleFirstModal}
                />
            </div>
        );

        return (
            <div className="App">
                <Header
                    selectedAutos={this.state.selectAutos}
                    autosInBasket={this.state.autosBasket}
                />

                <div className="containerContent">
                    <ListAutos
                        autos={this.state.autos}
                        handleModal={this.handleFirstModal}
                        addToSelected={this.addToSelected}
                        addAuto={this.addAuto}
                        selectedAutos={this.state.selectAutos}
                    />
                    <Modal
                        header="Modal"
                        closeButton={true}
                        text="Does it add to basket ?"
                        viewModal={this.state.viewFirstModal}
                        actions={btnF}
                        handlModal={this.handleFirstModal}
                    />
                </div>
            </div>
        );
    }
}

export default App;
