import { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './button.module.scss';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            backgroundColor,
            text,
            handlModal,
            addToBasket = Function.prototype,
            autoState,
        } = this.props;

        return (
            <div className="wrap-btn">
                <button
                    className={styles.btn}
                    style={{ backgroundColor: `${backgroundColor}`, margin: '10px' }}
                    onClick={() => {
                        addToBasket(autoState);
                        handlModal();
                    }}
                >
                    {text}
                </button>
            </div>
        );
    }
}

export default Button;

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    handlModal: PropTypes.func.isRequired,
    addToBasket: PropTypes.func,
    autoState: PropTypes.object,
};
