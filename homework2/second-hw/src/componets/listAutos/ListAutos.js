import { Component } from 'react';
import PropTypes from 'prop-types';
import { Auto } from '../auto/Auto';
import Spinner from '../spinner/Spinner';
import styles from './listAutos.module.scss';
class ListAutos extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { autos, addToSelected, addAuto, selectedAutos } = this.props;
        return !autos.length ? (
            <Spinner />
        ) : (
            <ul className={styles.listAutos}>
                {autos.map((auto) => {
                    return (
                        <Auto
                            key={auto.id}
                            auto={auto}
                            handleModal={this.props.handleModal}
                            addToSelected={addToSelected}
                            addAuto={addAuto}
                            selectedAutos={selectedAutos}
                        />
                    );
                })}
            </ul>
        );
    }
}
export { ListAutos };

ListAutos.propTypes = {
    autos: PropTypes.array.isRequired,
    addToSelected: PropTypes.func.isRequired,
    addAuto: PropTypes.func.isRequired,
    selectedAutos: PropTypes.array.isRequired,
};
