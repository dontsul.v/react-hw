import { Component } from 'react';
import PropTypes from 'prop-types';
import { BsBasketFill } from 'react-icons/bs';
import { BsFillStarFill } from 'react-icons/bs';
import styles from './header.module.scss';

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { selectedAutos, autosInBasket } = this.props;

        return (
            <nav className=" teal lighten-1 ">
                <div className="container nav-wrapper">
                    <a href="#" className="brand-logo">
                        Logo
                    </a>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li>
                            <a href="!#">
                                <BsBasketFill className={styles.icon} />
                                <span>
                                    {autosInBasket.length !== 0 ? autosInBasket.length : null}
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="!#">
                                <BsFillStarFill className={styles.icon} />
                                <span>
                                    {selectedAutos.length !== 0 ? selectedAutos.length : null}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}
export { Header };

Header.propTypes = {
    selectedAutos: PropTypes.array.isRequired,
    autosInBasket: PropTypes.array.isRequired,
};
