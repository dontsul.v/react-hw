import { setItemsBasket } from '../slices/autoBasketSlice';
import { setItemsSelected } from '../slices/autoSelectedSlice';

export const getDatas = () => (dispatch) => {
    const selectAutosStorage = localStorage.getItem('selectAutos');

    if (selectAutosStorage) {
        dispatch(setItemsSelected(JSON.parse(selectAutosStorage)));
    }
    const basketAutosStorage = localStorage.getItem('autosBasket');
    if (basketAutosStorage) {
        dispatch(setItemsBasket(JSON.parse(basketAutosStorage)));
    }
};
