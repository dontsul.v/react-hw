import { configureStore } from '@reduxjs/toolkit';
import { autosReducer } from '../slices/autosSlice';
import { autoSelectedReducer } from '../slices/autoSelectedSlice';
import { autoBasketReducer } from '../slices/autoBasketSlice';
import { autoStateReducer } from '../slices/autoStateSlice';
export const store = configureStore({
    reducer: {
        autos: autosReducer,
        autoSelected: autoSelectedReducer,
        autoBasket: autoBasketReducer,
        autoState: autoStateReducer,
    },
});
