import { createSlice } from '@reduxjs/toolkit';

const autoSelectedSlice = createSlice({
    name: 'autoSelected',
    initialState: {
        itemsSelected: [],
        loading: 'idle',
        error: null,
    },
    reducers: {
        toggleSelected: (state, action) => {
            const res = state.itemsSelected.findIndex((obj) => obj.id === action.payload.id);
            if (res === -1) {
                state.itemsSelected.push(action.payload);
            } else {
                state.itemsSelected = state.itemsSelected.filter(
                    (auto) => auto.id !== action.payload.id
                );
            }
            localStorage.setItem('selectAutos', JSON.stringify([...state.itemsSelected]));
        },
        setItemsSelected: (state, action) => {
            state.itemsSelected = action.payload;
        },
    },
});

export const autoSelectedReducer = autoSelectedSlice.reducer;
export const { toggleSelected, setItemsSelected } = autoSelectedSlice.actions;
