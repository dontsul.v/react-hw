import { MdDelete } from 'react-icons/md';
import styles from './elemBasket.module.scss';
import { toggleModalStatus } from '../../slices/autoBasketSlice';
import { useDispatch } from 'react-redux';
import { addAutoState } from '../../slices/autoStateSlice';
const ElemBasket = (props) => {
    const dispatch = useDispatch();
    const { id, name, price, img, color, quantity = 0 } = props.auto;
    return (
        <li className={styles.elemBasket}>
            <div>
                <img className={styles.img} src={img} alt="auto" />
            </div>
            <div>
                <h4>{name}</h4>
                <p>Color: {color}</p>
            </div>
            <div>
                <h5>Price: {price}$</h5>
            </div>
            <div>
                <h5>Quantity: {quantity}</h5>
            </div>
            <div>
                <MdDelete
                    className={styles.icon}
                    onClick={(e) => {
                        dispatch(toggleModalStatus(true));
                        dispatch(addAutoState(props.auto));
                    }}
                />
            </div>
        </li>
    );
};

export default ElemBasket;
