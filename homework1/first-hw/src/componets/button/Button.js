import { Component } from 'react';
import styles from './button.module.scss';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { backgroundColor, text, handlModal } = this.props;

        return (
            <div className="wrap-btn">
                <button
                    className={styles.btn}
                    style={{ backgroundColor: `${backgroundColor}`, margin: '10px' }}
                    onClick={() => handlModal()}
                >
                    {text}
                </button>
            </div>
        );
    }
}

export default Button;
