import { Component } from 'react';
import { CgCloseR } from 'react-icons/cg';
import styles from './modal.module.scss';
import React from 'react';
class Modal extends Component {
    constructor(props) {
        super(props);
        this.modal = React.createRef();
    }

    render() {
        const { header, closeButton, text, viewModal, handlModal, actions } = this.props;

        return (
            viewModal && (
                <div
                    ref={this.modal}
                    onClick={(e) => {
                        if (e.target === this.modal.current) {
                            handlModal();
                        }
                    }}
                    className={styles.modalBackground}
                >
                    <div className={styles.modal}>
                        <div className={styles.modalContent}>
                            <h3 className={styles.title}>{header}</h3>
                            {closeButton && (
                                <CgCloseR onClick={handlModal} className={styles.icon} />
                            )}
                            <p className={styles.text}>{text}</p>
                            <div className={styles.btnGroup}>{actions}</div>
                        </div>
                    </div>
                </div>
            )
        );
    }
}

export default Modal;
