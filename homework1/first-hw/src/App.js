import Button from './componets/button/Button';
import Modal from './componets/modal/Modal';
import styles from './App.module.scss';
import { Component } from 'react';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewFirstModal: false,
            viewSecondModal: false,
        };
    }

    handleFirstModal = (e) => {
        this.setState({
            viewFirstModal: !this.state.viewFirstModal,
        });
    };
    handleSecondModal = (e) => {
        this.setState({
            viewSecondModal: !this.state.viewSecondModal,
        });
    };

    render() {
        const btnF = (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Button text="Ok" backgroundColor="#d44637" handlModal={this.handleFirstModal} />
                <Button
                    text="Cancel"
                    backgroundColor="#d44637"
                    handlModal={this.handleFirstModal}
                />
            </div>
        );
        const btnS = (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Button text="Ok" backgroundColor="aqua" handlModal={this.handleSecondModal} />
                <Button text="Cancel" backgroundColor="aqua" handlModal={this.handleSecondModal} />
            </div>
        );

        return (
            <div className="App">
                <div className={styles.wrapBtn}>
                    <Button
                        text="Open first modal"
                        backgroundColor="red"
                        handlModal={this.handleFirstModal}
                    />
                    <Button
                        text="Open second modal"
                        backgroundColor="green"
                        handlModal={this.handleSecondModal}
                    />
                </div>
                <Modal
                    header="header some"
                    closeButton={true}
                    text="text"
                    viewModal={this.state.viewFirstModal}
                    actions={btnF}
                    handlModal={this.handleFirstModal}
                />
                <Modal
                    header="some header 2"
                    closeButton={true}
                    text="some text 2"
                    viewModal={this.state.viewSecondModal}
                    actions={btnS}
                    handlModal={this.handleSecondModal}
                />
            </div>
        );
    }
}

export default App;
