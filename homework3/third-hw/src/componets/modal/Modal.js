import { useRef } from 'react';
import PropTypes from 'prop-types';
import { CgCloseR } from 'react-icons/cg';
import styles from './modal.module.scss';
import React from 'react';

function Modal(props) {
    const modal = useRef(null);
    const { header, closeButton, text, viewModal, handlModal, actions } = props;

    return (
        viewModal && (
            <div
                ref={modal}
                onClick={(e) => {
                    if (e.target === modal.current) {
                        handlModal();
                    }
                }}
                className={styles.modalBackground}
            >
                <div className={styles.modal}>
                    <div className={styles.modalContent}>
                        <h3 className={styles.title}>{header}</h3>
                        {closeButton && <CgCloseR onClick={handlModal} className={styles.icon} />}
                        <p className={styles.text}>{text}</p>
                        <div className={styles.btnGroup}>{actions}</div>
                    </div>
                </div>
            </div>
        )
    );
}

export default Modal;

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
    viewModal: PropTypes.bool.isRequired,
    handlModal: PropTypes.func.isRequired,
    // actions: PropTypes.element.isRequired,
};
