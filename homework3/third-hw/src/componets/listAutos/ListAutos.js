import PropTypes from 'prop-types';
import { Auto } from '../auto/Auto';
import Spinner from '../spinner/Spinner';
import styles from './listAutos.module.scss';

function ListAutos(props) {
    const { autos, addToSelected, addAuto, selectedAutos, handleModal } = props;
    return !autos.length ? (
        <Spinner />
    ) : (
        <div className="container">
            <ul className={styles.listAutos}>
                {autos.map((auto) => {
                    return (
                        <Auto
                            key={auto.id}
                            auto={auto}
                            handleModal={handleModal}
                            addToSelected={addToSelected}
                            addAuto={addAuto}
                            selectedAutos={selectedAutos}
                        />
                    );
                })}
            </ul>
        </div>
    );
}

export { ListAutos };

ListAutos.propTypes = {
    autos: PropTypes.array.isRequired,
    addToSelected: PropTypes.func.isRequired,
    addAuto: PropTypes.func.isRequired,
    selectedAutos: PropTypes.array.isRequired,
};
