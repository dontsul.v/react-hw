import { useState } from 'react';
import Modal from '../modal/Modal';
import Button from '../button/Button';
import ElemBasket from '../elemBasket/ElemBasket';
import styles from './basket.module.scss';

const Basket = (props) => {
    const [viewDeleteModal, setViewDeleteModal] = useState(false);
    const { autosBasket, deleteFromBasket } = props;
    const cartEmpty = <h2 style={{ textAlign: 'center' }}>Cart is empty</h2>;

    const handleDeleteModal = () => {
        setViewDeleteModal(!viewDeleteModal);
    };

    const btnS = (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Button
                deleteFromBasket={() => deleteFromBasket(props.autoState.id)}
                text="Ok"
                backgroundColor="#d44637"
                handlModal={() => handleDeleteModal()}
                autoState={props.autoState}
            />
            <Button
                text="Cancel"
                backgroundColor="#d44637"
                handlModal={() => handleDeleteModal()}
            />
        </div>
    );
    return (
        <div className="container">
            {autosBasket.length !== 0 ? (
                <ul className={styles.listAutos}>
                    {autosBasket.map((elem) => {
                        return (
                            <ElemBasket
                                key={elem.id}
                                auto={elem}
                                handleDeleteModal={handleDeleteModal}
                                addAuto={props.addAuto}
                            />
                        );
                    })}
                </ul>
            ) : (
                cartEmpty
            )}

            {
                <Modal
                    header="Modal"
                    closeButton={true}
                    text="
                Are you sure you want to delete this element?"
                    viewModal={viewDeleteModal}
                    actions={btnS}
                    handlModal={handleDeleteModal}
                />
            }
        </div>
    );
};

export default Basket;
