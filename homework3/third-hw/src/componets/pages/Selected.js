import ElemSelect from '../elemSelect/ElemSelect';
import styles from './basket.module.scss';
const Selected = (props) => {
    const { selectedAutos, addToSelected } = props;
    const cartEmpty = <h2 style={{ textAlign: 'center' }}>Еhere are no items</h2>;
    return (
        <div className="container">
            {selectedAutos.length !== 0 ? (
                <ul className={styles.listAutos}>
                    {selectedAutos.map((elem) => {
                        return (
                            <ElemSelect
                                key={elem.id}
                                auto={elem}
                                addToSelected={addToSelected}
                                selectedAutos={props.selectedAutos}
                            />
                        );
                    })}
                </ul>
            ) : (
                cartEmpty
            )}
        </div>
    );
};

export default Selected;
