import { MdDelete } from 'react-icons/md';
import styles from './elemBasket.module.scss';
const ElemBasket = (props) => {
    const { id, name, price, img, color, quantity = 0 } = props.auto;
    return (
        <li className={styles.elemBasket}>
            <div>
                <img className={styles.img} src={img} alt="auto" />
            </div>
            <div>
                <h4>{name}</h4>
                <p>Color: {color}</p>
            </div>
            <div>
                <h5>Price: {price}$</h5>
            </div>
            <div>
                <h5>Quantity: {quantity}</h5>
            </div>
            <div>
                <MdDelete
                    className={styles.icon}
                    onClick={(e) => {
                        props.addAuto(props.auto);
                        props.handleDeleteModal();
                    }}
                />
            </div>
        </li>
    );
};

export default ElemBasket;
