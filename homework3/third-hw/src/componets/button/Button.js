import PropTypes from 'prop-types';
import styles from './button.module.scss';

function Button(props) {
    const {
        backgroundColor,
        text,
        handlModal = Function.prototype,
        addToBasket = Function.prototype,
        autoState = {},
        deleteFromBasket = Function.prototype,
    } = props;

    return (
        <div className="wrap-btn">
            <button
                className={styles.btn}
                style={{ backgroundColor: `${backgroundColor}`, margin: '10px' }}
                onClick={() => {
                    addToBasket({ ...autoState, quantity: 1 }, autoState.id);
                    deleteFromBasket();
                    handlModal();
                }}
            >
                {text}
            </button>
        </div>
    );
}

export default Button;

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    handlModal: PropTypes.func,
    addToBasket: PropTypes.func,
    autoState: PropTypes.object,
};
