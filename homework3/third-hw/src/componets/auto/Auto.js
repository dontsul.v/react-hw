import PropTypes from 'prop-types';
import { AiFillStar } from 'react-icons/ai';
import styles from './auto.module.scss';

function Auto(props) {
    const { addToSelected, addAuto, selectedAutos, handleModal } = props;
    const { id, name, price, img, color } = props.auto;

    const res = selectedAutos.findIndex((obj) => obj.id === id);
    let classes = styles.star;
    if (res === -1) {
        classes = styles.star;
    } else {
        classes = styles.star + ' ' + styles.activeStar;
    }

    return (
        <li>
            <div className="card" style={{ display: 'flex', flexDirection: 'column' }}>
                <div className="card-image">
                    <img src={img} style={{ maxWidth: '100%', height: '200px' }} />
                </div>
                <div className="card-content" style={{ flexGrow: '1' }}>
                    <span className="card-title">{name}</span>
                    <span>Color: {color}</span>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, earum.</p>
                    <div className={styles.wrapFoot}>
                        <AiFillStar
                            onClick={() => {
                                addToSelected(props.auto, id);
                            }}
                            className={classes}
                        />{' '}
                        <span className={styles.price}>${price}</span>
                        <button
                            onClick={() => {
                                handleModal();
                                addAuto(props.auto);
                            }}
                            className="waves-effect waves-light btn"
                        >
                            Add to basket
                        </button>
                    </div>
                </div>
            </div>
        </li>
    );
}

export { Auto };

Auto.propTypes = {
    selectedAutos: PropTypes.array.isRequired,
    addToSelected: PropTypes.func.isRequired,
    addAuto: PropTypes.func.isRequired,
};
