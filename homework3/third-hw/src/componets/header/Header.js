import { Link, NavLink, Outlet } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BsBasketFill } from 'react-icons/bs';
import { BsFillStarFill } from 'react-icons/bs';
import styles from './header.module.scss';
import { useState } from 'react';

function Header(props) {
    // const [quantity, setQuantity] = useState(0);
    const { selectedAutos, autosInBasket } = props;

    const allQuantity = autosInBasket.reduce((acc, order) => {
        return order.quantity + acc;
    }, 0);

    return (
        <>
            <nav className=" teal lighten-1 ">
                <div className="container nav-wrapper">
                    <Link to="/" className="brand-logo">
                        Logo
                    </Link>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li style={{ marginRight: '15px' }}>
                            <NavLink to="/">
                                <span style={{ fontWeight: 'bold' }}>Home</span>
                            </NavLink>
                        </li>
                        <li style={{ marginRight: '15px' }}>
                            <NavLink to="basket">
                                <span
                                    style={{
                                        marginRight: '10px',
                                        fontWeight: 'bold',
                                    }}
                                >
                                    Basket
                                </span>
                                <BsBasketFill className={styles.icon} />
                                <span
                                    style={{
                                        marginLeft: '5px',
                                        fontWeight: 'bold',
                                    }}
                                >
                                    {allQuantity !== 0 ? allQuantity : null}
                                </span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="selected">
                                <span style={{ marginRight: '8px', fontWeight: 'bold' }}>
                                    Selected
                                </span>
                                <BsFillStarFill className={styles.icon} />
                                <span style={{ marginLeft: '5px', fontWeight: 'bold' }}>
                                    {selectedAutos.length !== 0 ? selectedAutos.length : null}
                                </span>
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
            <Outlet />
        </>
    );
}

export { Header };

Header.propTypes = {
    selectedAutos: PropTypes.array.isRequired,
    autosInBasket: PropTypes.array.isRequired,
};
