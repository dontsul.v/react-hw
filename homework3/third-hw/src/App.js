import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Home from './componets/pages/Home';
import Basket from './componets/pages/Basket';
import Selected from './componets/pages/Selected';
import Button from './componets/button/Button';
import Modal from './componets/modal/Modal';
import { Header } from './componets/header/Header';

function App() {
    const [autos, setAutos] = useState([]);
    const [autosBasket, setAutosBasket] = useState([]);
    const [selectAutos, setSelectAutos] = useState([]);
    const [autoState, setAutoState] = useState({});
    const [viewFirstModal, setViewFirstModal] = useState(false);

    useEffect(() => {
        fetch('MOCK_DATA.json')
            .then((res) => res.json())
            .then((data) => {
                setAutos(data);
            });
        const selectAutosStorage = localStorage.getItem('selectAutos');
        if (selectAutosStorage) {
            setSelectAutos(JSON.parse(selectAutosStorage));
        }
        const basketAutosStorage = localStorage.getItem('autosBasket');
        if (basketAutosStorage) {
            setAutosBasket(JSON.parse(basketAutosStorage));
        }
    }, []);

    const addToSelected = (auto, id) => {
        const res = selectAutos.findIndex((obj) => obj.id === id);
        if (res === -1) {
            setSelectAutos([...selectAutos, auto]);
            localStorage.setItem('selectAutos', JSON.stringify([...selectAutos, auto]));
        } else {
            const arr = selectAutos.filter((isAuto) => isAuto.id !== id);
            setSelectAutos([...arr]);
            localStorage.setItem('selectAutos', JSON.stringify(arr));
        }
    };

    const addToBasket = (auto, id) => {
        const res = autosBasket.findIndex((obj) => obj.id === id);

        if (res === -1) {
            setAutosBasket([...autosBasket, auto]);
            localStorage.setItem('autosBasket', JSON.stringify([...autosBasket, auto]));
        } else {
            autosBasket[res].quantity = Number(autosBasket[res].quantity) + 1;
            setAutosBasket([...autosBasket]);
            localStorage.setItem('autosBasket', JSON.stringify([...autosBasket]));
        }
    };

    const deleteFromBasket = (id) => {
        const filteredAuto = autosBasket.filter((elem) => elem.id !== id);
        setAutosBasket([...filteredAuto]);
        localStorage.setItem('autosBasket', JSON.stringify([...filteredAuto]));
    };

    const addAuto = (auto) => {
        setAutoState(auto);
    };

    const handleFirstModal = (e) => {
        setViewFirstModal(!viewFirstModal);
    };

    const btnF = (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Button
                addToBasket={addToBasket}
                text="Ok"
                backgroundColor="#d44637"
                handlModal={handleFirstModal}
                autoState={autoState}
            />
            <Button text="Cancel" backgroundColor="#d44637" handlModal={handleFirstModal} />
        </div>
    );

    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route
                        path="/"
                        element={<Header selectedAutos={selectAutos} autosInBasket={autosBasket} />}
                    >
                        <Route
                            index
                            element={
                                <Home
                                    autos={autos}
                                    handleModal={handleFirstModal}
                                    addToSelected={addToSelected}
                                    addAuto={addAuto}
                                    selectedAutos={selectAutos}
                                />
                            }
                        />

                        <Route
                            path="basket"
                            element={
                                <Basket
                                    autosBasket={autosBasket}
                                    deleteFromBasket={deleteFromBasket}
                                    setViewFirstModal={setViewFirstModal}
                                    viewFirstModal={viewFirstModal}
                                    autoState={autoState}
                                    addAuto={addAuto}
                                />
                            }
                        ></Route>
                        <Route
                            path="selected"
                            element={
                                <Selected
                                    addToSelected={addToSelected}
                                    selectedAutos={selectAutos}
                                />
                            }
                        ></Route>
                    </Route>
                </Routes>
            </BrowserRouter>

            <Modal
                header="Modal"
                closeButton={true}
                text="Does it add to basket ?"
                viewModal={viewFirstModal}
                actions={btnF}
                handlModal={handleFirstModal}
            />
        </div>
    );
}

export default App;
