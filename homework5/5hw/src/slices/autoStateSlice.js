import { createSlice } from '@reduxjs/toolkit';

const autoStateSlice = createSlice({
    name: 'autoState',
    initialState: {},
    reducers: {
        addAutoState: (state, action) => {
            state.autoState = action.payload;
        },
    },
});

export const autoStateReducer = autoStateSlice.reducer;
export const { addAutoState } = autoStateSlice.actions;
