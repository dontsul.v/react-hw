import { useField } from 'formik';
import { PatternFormat } from 'react-number-format';
import styles from './phoneInput.module.scss';

export function PhoneInput({ label, ...props }) {
    const [field, meta] = useField(props);
    return (
        <>
            <label htmlFor={field.name}>{label}</label>
            <PatternFormat
                format="(###) ###-####"
                mask="_"
                allowEmptyFormatting
                {...props}
                {...field}
            />
            {meta.touched && meta.error ? <div className={styles.error}>{meta.error}</div> : null}
        </>
    );
}
