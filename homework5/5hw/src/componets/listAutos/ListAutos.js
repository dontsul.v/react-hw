import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Auto } from '../auto/Auto';
import Spinner from '../spinner/Spinner';
import styles from './listAutos.module.scss';
import { toggleModalStatus, addInBasket } from '../../slices/autoBasketSlice';
import Modal from '../modal/Modal';
import { useDispatch } from 'react-redux';

function ListAutos() {
    const autos = useSelector((state) => state.autos.items);
    const isLoading = useSelector((state) => state.autos.loading);
    const auto = useSelector((state) => state.autoState.autoState);
    const dispatch = useDispatch();
    return isLoading === 'loading' ? (
        <Spinner />
    ) : (
        <div className="container">
            <ul className={styles.listAutos}>
                {autos.map((auto) => {
                    return <Auto key={auto.id} auto={auto} />;
                })}
            </ul>
            <Modal
                header="Modal"
                closeButton={true}
                text="Does it add to basket ?"
                viewModal={toggleModalStatus}
                handleBasket={() => {
                    dispatch(addInBasket(auto));
                }}
            />
        </div>
    );
}

export { ListAutos };

// ListAutos.propTypes = {
//     autos: PropTypes.array.isRequired,
//     addToSelected: PropTypes.func.isRequired,
//     addAuto: PropTypes.func.isRequired,
//     selectedAutos: PropTypes.array.isRequired,
// };
