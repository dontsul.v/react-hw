import { ListAutos } from '../listAutos/ListAutos';

const Home = (props) => {
    const { autos, addToSelected, addAuto, selectedAutos, handleModal } = props;
    return (
        <div>
            <ListAutos
                autos={autos}
                handleModal={handleModal}
                addToSelected={addToSelected}
                addAuto={addAuto}
                selectedAutos={selectedAutos}
            />
        </div>
    );
};

export default Home;
