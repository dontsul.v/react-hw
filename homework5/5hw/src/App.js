import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Home from './componets/pages/Home';
import Basket from './componets/pages/Basket';
import Selected from './componets/pages/Selected';
import { Header } from './componets/header/Header';
import { useDispatch } from 'react-redux';
import { fetchData } from './slices/autosSlice';
import { getDatas } from './thunks/getDatas';
function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchData());
        dispatch(getDatas());
        // const selectAutosStorage = localStorage.getItem('selectAutos');
        // if (selectAutosStorage) {
        //     setSelectAutos(JSON.parse(selectAutosStorage));
        // }
        // const basketAutosStorage = localStorage.getItem('autosBasket');
        // if (basketAutosStorage) {
        //     setAutosBasket(JSON.parse(basketAutosStorage));
        // }
    }, []);

    // const addToSelected = (auto, id) => {
    //     const res = selectAutos.findIndex((obj) => obj.id === id);
    //     if (res === -1) {
    //         setSelectAutos([...selectAutos, auto]);
    //         localStorage.setItem('selectAutos', JSON.stringify([...selectAutos, auto]));
    //     } else {
    //         const arr = selectAutos.filter((isAuto) => isAuto.id !== id);
    //         setSelectAutos([...arr]);
    //         localStorage.setItem('selectAutos', JSON.stringify(arr));
    //     }
    // };

    // const addToBasket = (auto, id) => {
    //     const res = autosBasket.findIndex((obj) => obj.id === id);

    //     if (res === -1) {
    //         setAutosBasket([...autosBasket, auto]);
    //         localStorage.setItem('autosBasket', JSON.stringify([...autosBasket, auto]));
    //     } else {
    //         autosBasket[res].quantity = Number(autosBasket[res].quantity) + 1;
    //         setAutosBasket([...autosBasket]);
    //         localStorage.setItem('autosBasket', JSON.stringify([...autosBasket]));
    //     }
    // };

    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Header />}>
                        <Route index element={<Home />} />
                        <Route path="basket" element={<Basket />}></Route>
                        <Route path="selected" element={<Selected />}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
